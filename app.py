from flask import Flask
from square import squarefn

app = Flask(__name__) # instanciation de la classe Flask

@app.route("/")
def home():
    return "Page d'accueil\n"

@app.route("/square/<number>")
def getSquare(number):
    res = None # réponse

    try:
        n = float(number)
    except:
        return "Problème..."

    try:
        res = squarefn(n)
    except:
        return "Problème..."

    return str(res) # la réponse a une requête http doit être une str



# démarrage du serveur
app.run(host="0.0.0.0", port=8083)





