def squarefn(n):
    if type(n) not in [int, float]:
        raise TypeError("Le calcul du carré est possible seulement sur un nombre réel")
    return n*n