import unittest
from square import squarefn

class TestSquareModule(unittest.TestCase):
    def test_square(self):
        self.assertEqual(squarefn(5), 25)
    
    def test_types(self):
        self.assertRaises(TypeError, squarefn, True)
        self.assertRaises(TypeError, squarefn,"5.2")
